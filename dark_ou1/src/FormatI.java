import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;

import static java.lang.Math.toIntExact;

public class FormatI {

    private int op;
    private long rs;
    private long rt;
    private long immediate;

    private String rsReg;
    private String rtReg;
    private PrintWriter pw;

    private Hashtable<Integer,String> formatIOp;
    private Hashtable<Integer, String> registerTable;
    private Hashtable<Integer,String> functions0x01;

    public FormatI(PrintWriter pw)
    {
        formatIOp = new Hashtable<Integer, String>();
        functions0x01 = new Hashtable<Integer, String>();
        registerTable = new Hashtable<Integer, String>();
        makeFunctionTable(formatIOp,"FormatIOp.txt");
        makeFunctionTable(functions0x01,"0x01.txt");
        makeFunctionTable(registerTable,"RegisterTable.txt");
        this.pw = pw;
    }

    private String getFunction(Hashtable<Integer,String>table, Integer key){
        String func = table.get(key);
        return func;
    }

    private void makeFunctionTable(Hashtable<Integer,String> functions,String txt)  {
        String currentLine;
        String[] tmp;
        try {
            BufferedReader funcFile = new BufferedReader(new FileReader(txt));
            while( ( currentLine = funcFile.readLine()) != null ){
                tmp = currentLine.split(" ");
                int key = Integer.parseInt(tmp[0]);
                functions.put(key,tmp[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printInstrFormat(){

        immediate = getImmediate(immediate);
        if( op == 1 ){
            //print instr rs, immi
            rsReg = registerTable.get(toIntExact(rs));
            String instruction = getFunction( functions0x01, (int)rt );
            pw.println(instruction+" "+rsReg+" " + immediate );

        }
        else if(rt != 0) {
            /*print standard output which is
             *instr rt, rs, immi
            */
            rsReg = registerTable.get(toIntExact(rs));
            rtReg = registerTable.get(toIntExact(rt));
            String instruction = getFunction(formatIOp, op);
            if (instruction.endsWith("w") || instruction.endsWith("wr") ||
                    instruction.endsWith("wl")) {

                pw.println(instruction + " " + rtReg + " " +
                        "" + immediate + "(" + rsReg + ") ");

            } else {
                if (instruction.startsWith("b")) {
                    programCounter(immediate);
                    pw.println(instruction + " " + rsReg + " " + rtReg + " " + immediate);
                } else {

                    pw.println(instruction + " " + rtReg + " " + rsReg + " " + immediate);
                }
            }
        }
        else{
            rtReg = registerTable.get(toIntExact(rt));
            String instruction = getFunction( formatIOp, op );
            if(instruction == null)
            {
                pw.printf("Does not exist\n");
                return;
            }
            //print instr rt, immi
            if (instruction.startsWith("b")) {
                programCounter(immediate);
                pw.println(instruction + " " + rsReg + " " + rtReg + " " + immediate);
            } else {
                pw.println(instruction+" "+rtReg+" " + immediate );
            }
        }

    }

    /**
     *
     * @param immi the immediate to check for signedness and value
     * @return the negative value of immi or if it's not negative the positive.
     */
    private long getImmediate(long immi){

        String binString = Long.toBinaryString(immi);
        short number = (short)immi;
        if( binString.charAt(0) == '1' ){ // then leading one means negative num

            number = (short)Integer.parseInt(binString,2);
        }
        return number;
    }

    public void decode(int opNr, String code) {
        op = opNr;
        long raw = Long.decode(code);
        long bitmask = 0x03E00000;
        rs = (raw & bitmask) >> 21;

        bitmask = 0x001F0000;
        rt = (raw & bitmask) >> 16;

        bitmask = 0x0000FFFF;
        immediate = (raw & bitmask);

        rsReg = registerTable.get(toIntExact(rs));
        rtReg = registerTable.get(toIntExact(rt));

        //if the immediate is negative, convert it to the right value. Else
        // nothing

        pw.print(code + "\tI\t");
        pw.printf("[%-2s %-2s %-2s %-5s] ",op,rs,rt,immediate);
        pw.printf("\t \t[%-2s %-2s %-2s %-4s] \t",Long.toHexString(op),Long
                        .toHexString(rs),
                Long.toHexString(rt),Long.toHexString(immediate));
        printInstrFormat();
    }
//no comment
    private void programCounter(long immi){

        long remainder = immi % 4;
        remainder= 4- remainder;

        immediate += remainder;
    }
}