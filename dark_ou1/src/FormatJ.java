import java.io.PrintWriter;

/**
 * Created by c14rdo on 2016-11-11.
 */
public class FormatJ {
    private int op;
    private long jump_target;

    private final int j = 2;
    private final int jal = 3;
    private PrintWriter pw;

    public FormatJ(PrintWriter pw){

        this.pw = pw;
    }

    public void decode(int opNr, String code)
    {
        op = opNr;
        long raw = Long.decode(code);
        long bitmask = 0x01FFFFFF;
        jump_target = (raw & bitmask);

        pw.print(code + "\tJ\t");
        pw.printf("[%-2s %-5s] ",op, jump_target);
        pw.printf("\t\t\t[%-2s %-4s]\t\t\t",
                Long.toHexString(op),Long.toHexString(jump_target));
/*
        System.out.print(code + "\tJ\t");
        System.out.printf("[%-2s %-5s] ",op, jump_target);
        System.out.printf("\t\t\t[%-2s %-4s]\t\t\t",Long.toHexString(op),
                Long.toHexString(jump_target));
*/
        switch (op)
        {
            case j:
                pw.println("j " + jump_target);
  //              System.out.println("j " + jump_target);
                break;
            case jal:
                jump_target = jump_target <<2;
                pw.println("jal " + Long.toHexString(jump_target) );
          //              System.out.println("jal " + Long.toHexString
              //      (jump_target));
                break;
            default:
                pw.println("Given input was not a valid operation.");
       //         System.out.println("Given input was not a valid operation.");
                break;
        }
    }
}
