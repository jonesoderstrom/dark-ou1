import java.io.PrintWriter;

/**
 * Class to parse assembler hexadecimal code
 * Created by c14rdo on 2016-11-11.
 */
public class ParseCodes {

    private int op;
    private String instruction;
    private long rs;
    private long rt;
    private long rd;
    private long shamt;
    private long func;

    private FormatR typeR = null;
    private FormatJ typeJ = null;
    private FormatI typeI = null;
    private PrintWriter pw;

    public ParseCodes(FormatR r, FormatI i, FormatJ j,PrintWriter pw){

        //retrieve and save format classes from caller.
        typeR = r;
        typeI = i;
        typeJ = j;

        //retrieve the file writing object
        this.pw = pw;

        //make column headers
        pw.println("Instruction\tFmt\tDecomp deci\t\t\t\tDecomp " +
                "hex  \t\tSource");

    }

//lagt till en kommentar
    /**
     * Reads the 6 leftmost bits in the instruction
     * @param operation containing the whole instruciton in hexadecimal
     */
    private void readNewOp(String operation){

        instruction = operation;
        String test = "0x71014802";
        long decodedOp = Long.decode(operation);
        long bitmask = 0xFC000000;
        decodedOp = (decodedOp & bitmask)  >> 26 ;

        op = Integer.parseInt(Long.toString(decodedOp) );

        /*System.out.println("Type has op = "+Long.toBinaryString(decodedOp) +
                " And " +
                "in hex = " +Long.toHexString(decodedOp));*/
    }

    public void sendToFormatHandler(String operation){

        readNewOp(operation);

        switch(op ){

            case 0: //handle some more
                typeR.decode(op, instruction);
                break;

            case 1:
                typeI.decode(op,instruction);
                break;

            case 2:
                typeJ.decode(op, instruction);
                break;
            case 3:
                typeJ.decode(op, instruction);
                break;
            case 28:// op 1c all are type R
                typeR.decode(op, instruction);
                break;

            default://not in one of the tables pointed to by the above op codes.
                typeI.decode(op, instruction);
                break;
        }
    }


}
