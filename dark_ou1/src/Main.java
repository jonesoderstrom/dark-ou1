import java.io.*;

/**
 * Created by c14rdo on 2016-11-11.
 */
public class Main {


    public static void main(String[] args) {

        String file = "codes.txt";

        if(args.length > 0){
            file = args[0];
        }

        try{
            PrintWriter pw = new PrintWriter("ParsedInstructions.txt", "UTF-8");
            String currentLine;
            BufferedReader hexFile = new BufferedReader(
                                                new FileReader(file));
            FormatR r = new FormatR(pw);
            FormatI i = new FormatI(pw);
            FormatJ j = new FormatJ(pw);

            ParseCodes pc = new ParseCodes(r,i,j,pw);
            while( ( currentLine = hexFile.readLine()) != null ){
                pc.sendToFormatHandler(currentLine);
            }
            pw.close();

        }
        catch (IOException e){
            System.err.println("File doesn't exist or invalid file name!");
            e.printStackTrace();

        }
    }

}
