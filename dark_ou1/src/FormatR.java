import java.io.*;
import java.util.Hashtable;

import static java.lang.Math.toIntExact;

/**
 * Created by c14rdo on 2016-11-11.
 */



public class FormatR {
    private int op;
    private long rs;
    private long rt;
    private long rd;
    private long sa;
    private int func;
    private String function;
    private String regRs;
    private String regRt;
    private String regRd;
    private PrintWriter pw;

    private Hashtable<Integer,String> functions0x1c;
    private Hashtable<Integer,String> functions0x00;
    private Hashtable<Integer,String> registerTable;

    public FormatR(PrintWriter pw){
        functions0x1c = new Hashtable<Integer, String>();
        functions0x00 = new Hashtable<Integer, String>();
        registerTable = new Hashtable<Integer, String>();
        makeFunctionTable(functions0x1c,"0x1c.txt");
        makeFunctionTable(functions0x00,"0x00.txt");
        makeFunctionTable(registerTable,"RegisterTable.txt");
        this.pw = pw;

    }
    public void decode(int op, String code){

        long bitmask = 0x0000003f;
        long instruction = Long.decode(code);
        func = (int)(instruction&bitmask);

        bitmask = 0x03E00000;
        rs = (instruction & bitmask) >> 21;

        bitmask = 0x001F0000;
        rt = (instruction & bitmask) >> 16;

        bitmask = 0x0000F800;
        rd = (instruction & bitmask) >> 11;

        bitmask = 0x000007C0;
        sa = (instruction & bitmask) >> 6;




        regRs = registerTable.get(toIntExact(rs));
        regRt = registerTable.get(toIntExact(rt));
        regRd = registerTable.get(toIntExact(rd));

        pw.print(code + "\tR\t");
        pw.printf("[%-2s %-2s %-2s %-2s %-2s %-2s ] ",op,rs,rt,rd,sa,func);

        pw.printf("\t[%-2s %-2s %-2s %-2s %-2s %-2s] ",Long.toHexString(op),Long
                                         .toHexString(rs), Long.toHexString(rt),
                                      Long.toHexString(rd),Long.toHexString(sa),
                                                        Long.toHexString(func));

        /*
        System.out.printf("[%-2s %-2s %-2s %-2s %-2s %-2s ] ",op,rs,rt,rd,
                sa,func);
        System.out.printf("\t[%-2s %-2s %-2s %-2s %-2s %-2s] ",
                            Long.toHexString(op),Long.toHexString(rs),
                Long.toHexString(rt),Long.toHexString(rd),Long.toHexString(sa),
                                                        Long.toHexString(func));
        */

        this.op = op;
        switch (op) {
            case 0:
                function = getFunction(functions0x00,func);
                if (function == null)
                    pw.printf("Does not exist\n");
                else
                    printInstrFormat();
                break;
            case 28:
                function = getFunction(functions0x1c,func);
                if (function == null)
                    pw.printf("Does not exist\n");
                else
                    printInstrFormat();
                break;
        }
    }

    private void printInstrFormat(){
        if (!function.equals("srl") || !function.equals("sra") || !function.equals("sll") )
        {
            sa = 0;
        }
        if (op == 28 && sa == 0 && rt == 0){
            pw.println(function+" "+regRd+" " +regRs);
            //System.out.println(function+" "+regRd+" " +regRs);
        }
        else if(op == 0 && sa == 0 && rt == 0){
            if (function.equals("jr"))
            {
                pw.println(function+" "+regRs);
              //  System.out.println(function+" "+regRs);
            }
            else
                pw.println(function+" "+regRs+" " +regRd);
               // System.out.println(function+" "+regRs+" " +regRd);
        }
        else if (op == 0 && sa>0){
            pw.println(function + " " + regRd + " " + regRs + " " + sa);
           // System.out.println(function + " " + regRd + " " + regRs + " " +
            //        sa);

        }
        else if( op ==0  && sa == 0 && (func==6 || func == 7 || func == 4)){
            pw.println(function+" "+regRd+" " +regRt+" "+regRs);
            //System.out.println(function+" "+regRd+" " +regRt+" "+regRs );
        }
        else if((rd == 0 && sa == 0) && op == 0){
            pw.println(function+" "+regRs+" " +regRt);
            //System.out.println(function+" "+regRs+" " +regRt);
        }
        else if (rt == 0 && rd == 0 && sa == 0 ){
            pw.println(function+" "+regRs);
            //System.out.println(function+" "+regRs);
        }
        else if (sa == 0){
            pw.println(function+" "+regRd+" " +regRs+ " " + regRt);
            //System.out.println(function+" "+regRd+" " +regRs+ " " + regRt);
        }
        else{
            System.out.println("här blev det galet...");
        }
    }

    private String getFunction(Hashtable<Integer,String> ht,Integer key){
        String func = ht.get(key);
        return func;
    }
    private void makeFunctionTable(Hashtable<Integer,String> functions,String txt)  {
        String currentLine;
        String[] tmp;
        try {
            BufferedReader funcFile = new BufferedReader(new FileReader(txt));
            while( ( currentLine = funcFile.readLine()) != null ){
                tmp = currentLine.split(" ");
                int key = Integer.parseInt(tmp[0]);
                functions.put(key,tmp[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
